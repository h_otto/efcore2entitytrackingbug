using App;
using System;
using System.Linq;
using Xunit;

namespace Tests
{
    public class TestCases
    {
        /// <summary>
        /// This passes
        /// </summary>
        [Fact]
        public void EntityProjection()
        {
            SeedDatabase();

            using (var ctx = new MyDbContext())
            {
                var result = ctx.Parents.Select(p =>
                new
                {
                    Parent = p,
                    Children = p.Children
                })
                .First();

                Assert.Equal(2, result.Children.Count);
                Assert.Equal(result.Parent.Id, result.Children[0].ParentId);
                Assert.Equal(result.Parent.Id, result.Children[1].ParentId);
                Assert.Equal(2, ctx.ChangeTracker.Entries<Child>().Count());
                Assert.Same(result.Parent, result.Children[0].Parent);
                Assert.Same(result.Parent, result.Children[1].Parent);
            }
        }

        /// <summary>
        /// This fails
        /// </summary>
        [Fact]
        public void FilteredEntityProjection()
        {
            SeedDatabase();

            using (var ctx = new MyDbContext())
            {
                var result = ctx.Parents.Select(p =>
                new
                {
                    Parent = p,
                    Children = p.Children.Where(c => c.Name.Length > 0).ToList()
                })
                .First();

                Assert.Equal(2, result.Children.Count);
                Assert.Equal(result.Parent.Id, result.Children[0].ParentId);
                Assert.Equal(result.Parent.Id, result.Children[1].ParentId);
                //This line fails: 2 != 0
                Assert.Equal(2, ctx.ChangeTracker.Entries<Child>().Count());
            }
        }

        private static void SeedDatabase()
        {
            using (var ctx = new MyDbContext())
            {
                ctx.Database.EnsureDeleted();
                ctx.Database.EnsureCreated();
                var parent = new Parent();
                var child1 = new Child { Name = "Child1", Parent = parent };
                var child2 = new Child { Name = "Child2", Parent = parent };
                ctx.AddRange(child1, child2);
                Assert.Equal(3, ctx.ChangeTracker.Entries().Count());
                ctx.SaveChanges();
            }
        }
    }
}
